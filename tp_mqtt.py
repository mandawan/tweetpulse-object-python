#!/usr/bin/python

import settings
import os
import time
import tp_utils
import subprocess
import socket
from threading import Thread

class SubscribeMqtt(Thread):
    def __init__(self, mqtt_client):
        Thread.__init__(self)
        self.client = mqtt_client

    def run(self):
        try:
            self.connectToServer()
        except Exception, e:
            self.connectToServer()
            print("error because of : ", e)

    def stop(self):
        self.client.disconnect()

    # The callback for when the client receives a CONNACK response from the server.
    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result code "+str(rc))
        # Insert topics in a list
        topicList = []
        for topicIndex, topicValue in settings.TOPICS.iteritems():
            topicList.append((topicValue, 0))

        # Subscribe to all topics in one subscription
        self.client.subscribe(topicList)
        print "subscribe to : ", topicList

    # Callback on subscription
    def on_subscribe(self, client, userdata, flags, rc):
        print "publish init"
        print settings.TOPICS['init']
        # Publish init message
        self.client.publish(settings.TOPICS['init'], settings.CLIENT_ID, qos=0, retain=False)

    # The callback for when a PUBLISH message is received from the server.
    def on_message(self, client, userdata, msg):
        print(msg.topic+" "+str(msg.payload))
        # if it is a data message
        if msg.topic == settings.TOPICS['data']:
            search_queries = str(msg.payload)
            search_queries = search_queries.split(':')
            tp_utils.write_to_file(settings.DATA_FILES['mention_to_search'], search_queries[1])
            tp_utils.write_to_file(settings.DATA_FILES['lang_to_search'], search_queries[2])
            # Launch tweet stream
            tp_utils.launch_tweet_stream(search_queries[1], search_queries[2])

        # if it is a control message
        if msg.topic == settings.TOPICS['control']:
            message = str(msg.payload)
            splited_message = message.split(":")
            command = splited_message[0]
            if (command == "pulse_level"):
                #TODO relaunch cycle on pulse level change
                pulse_level = splited_message[1]
                tp_utils.write_to_file(settings.DATA_FILES['pulse_level'], str(pulse_level))
                settings.PULSE_LEVEL = pulse_level
                tp_utils.write_to_file(settings.DATA_FILES['mentions_counter'], str(0))
                print pulse_level

    def connectToServer(self):
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.on_subscribe = self.on_subscribe
        self.client.connect("web2.uzful.fr", 1883, 60)# web2.uzful.fr or 149.202.206.53
        self.client.loop_forever()
        # Blocking call that processes network traffic, dispatches callbacks and
        # handles reconnecting.
        # Other loop*() functions are available that give a threaded interface and a
        # manual interface.
