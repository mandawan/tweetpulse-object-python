#!/usr/bin/python

from tp_mqtt import SubscribeMqtt
from tp_btn import TPButton
from cycle_manager import CycleManager
import paho.mqtt.client as mqtt
import os
import time
import subprocess
import signal
import settings
import tp_utils
# import paho.mqtt.publish as publish

def init_data_files():
    for data_file in settings.DATA_FILES:
        # if file doesn't exist
        if not os.path.exists(settings.APP_DIR + "/datas/" + settings.DATA_FILES[data_file]):
            print ("creating file : ", settings.DATA_FILES[data_file])
            # Create it
            open(settings.APP_DIR + '/datas/' + settings.DATA_FILES[data_file], 'a')

def init_data_dir():
    # Verify needed directory exist
    if not os.path.isdir(settings.APP_DIR + "/datas"):
        print("create data dir and file")
        os.makedirs(settings.APP_DIR + "/datas")
    # Then verify needed files
    init_data_files()


if __name__ == '__main__':

    try:
        # TODO recup 3 dernier digit de l'adresse mac eth0 pour ID
        init_data_dir()
        button = TPButton()
        button.start()

        client = mqtt.Client()
        mqtt_sub = SubscribeMqtt(client)
        mqtt_sub.start()
        print "in try main"
        # settings.display.__init__()
        settings.display.lcd_string("  Connexion MQTT",settings.LCD_LINE_1)
        settings.display.lcd_string("     en cours...",settings.LCD_LINE_2)

        # cycle = CycleManager()
        # cycle.launch_cycle()

    except (KeyboardInterrupt, SystemExit):
        print "KeyboardInterrupt"
        os.system('pkill -9 -f tp_cycle.py')
        time.sleep(0.2)
        os.system('pkill -9 -f twitter_stream.py')
        settings.display.clean_display()
        os.system('kill %d' % os.getpid())
        # mqtt_sub.stop()
    # finally:
    #     settings.display.clean_display()
