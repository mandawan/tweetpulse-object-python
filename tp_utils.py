#!/usr/bin/python
import settings
import os
import time, calendar, datetime
import subprocess

def read_file(file_name):
    file = open(settings.APP_DIR + '/datas/' + file_name, 'r')
    file_content = file.read()
    file_content = file_content.rstrip('\n')
    return file_content

def write_to_file(file_name, content):
    file = open(settings.APP_DIR + '/datas/' + file_name, 'w')
    file.write(str(content))

def launch_tweet_stream(expression, lang):#expression, lang
    if not settings.is_streaming:
        # Start twitter stream
        print "start twitter stream"
        # settings.display.__init__()
        settings.display.lcd_string(" Connexion Twitter",settings.LCD_LINE_1)
        settings.display.lcd_string("    en cours...",settings.LCD_LINE_2)
        print "we are searching for : ", expression
        subprocess.Popen(["python","twitter_stream.py",expression, lang])
        settings.is_streaming = True
    else:
        print("new search, killing twitter_stream and relaunch")
        settings.display.__init__()
        settings.display.lcd_string("Nouvelle mention !", settings.LCD_LINE_1)
        settings.display.lcd_string("Chargement...", settings.LCD_LINE_2)
        write_to_file(settings.DATA_FILES['mentions_counter'], str(0))
        # kill current streaming and relaunch it
        # pkill enable us to kill a process without his PID, only filename
        os.system('pkill -9 -f twitter_stream.py')
        time.sleep(1)
        # maybe wait a bit
        subprocess.Popen(["python","twitter_stream.py",expression, lang])

def get_current_time_with_milli():
    current_date = datetime.datetime.now()
    current_unix_timestamp = calendar.timegm(time.gmtime())
    current_milli = int(current_date.microsecond/1e3)
    current_time_with_milli = str(current_unix_timestamp)+"."+str(current_milli)
    return current_time_with_milli

def last_mention_time_diff():
    current_time_with_milli = get_current_time_with_milli()
    last_mention_time = read_file(settings.DATA_FILES['last_mention_time'])
    print "current_time_milli : ", float(current_time_with_milli)
    print "last_mention_time : ", last_mention_time
    diff = float(current_time_with_milli) - float(last_mention_time)
    return diff

def read_pulse_level_var(var_type):
    pulse_level = read_file(settings.DATA_FILES['pulse_level'])
    splitted_pulse_level = pulse_level.split('/')
    if var_type == "time":
        pulse_level_time = splitted_pulse_level[1]
        if pulse_level_time == "m":
            pulse_level_time = 60
        elif pulse_level_time == "h":
            pulse_level_time = 3600
        elif pulse_level_time == "d":
            pulse_level_time = 86400
        return pulse_level_time
    elif var_type == "mention":
        pulse_level_mention = int(splitted_pulse_level[0])
        return pulse_level_mention

def check_alert():
    print("check_alert")
    last_mention_time = read_file(settings.DATA_FILES['last_mention_time'])
    diff = last_mention_time_diff()
    print("last_mention_time_diff() : ", diff )
    diff_minutes = diff / 60
    print("diff_minutes : ", str(diff_minutes))
    current_mention_per_min = 1 / diff_minutes
    print("current_mention_per_min : ", str(current_mention_per_min))
    pulse_level = read_pulse_level_var("mention")
    cycle_time = read_pulse_level_var("time")
    if(int(current_mention_per_min) >= pulse_level ):
        print "alerttttt"
        subprocess.Popen(["python","led.py"])
