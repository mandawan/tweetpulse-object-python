#!/usr/bin/python

import settings
import tp_utils
import sys
import time, calendar
import datetime
import schedule

print "tp cycle launch"

class TPCycle(object):

    def __init__(self):
        self.begin_cycle_time = calendar.timegm(time.gmtime())

    def run_cycle(self):
        # display search_queries on screen
        settings.display.display_mention_to_search()
        # settings.display.lcd_string("0" ,settings.LCD_LINE_2)
        tp_utils.write_to_file(settings.DATA_FILES['begin_cycle_time'], str(self.begin_cycle_time))
        tp_utils.write_to_file(settings.DATA_FILES['mentions_counter'], str(0))
        tp_utils.check_alert()

    def init_cycle(self):
        print "entrer init cycle"
        mention_val = tp_utils.read_pulse_level_var("mention")
        time_val = tp_utils.read_pulse_level_var("time")
        print("self.time_val : ", time_val)
        if time_val == 60:
            print "launch minute schedule"
            self.run_cycle()
            schedule.every(60).seconds.do(self.run_cycle)
        if time_val == 3600:
            print "launch hour schedule"
            self.run_cycle()
            schedule.every().hour.do(self.run_cycle)
        if time_val == 86400:
            now = datetime.datetime.now()
            now_time = now.hour+":"+now.minute
            print "launch day schedule"
            self.run_cycle()
            schedule.every().day.at(now_time).do(self.run_cycle)
        while True:
            schedule.run_pending()

    # TODO envoyer data vers le serveur avant de les mettre a 0

tp_cycle = TPCycle()
tp_cycle.init_cycle()
