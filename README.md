# tweetpulse-object-python

This project runs on a RaspberryPi B+

###Dependencies:
Simply run ```pip install -r requirements.txt``` to install all dependencies.

###Run:
Clone the project and ```cd``` to the directory
Run this command to launch project : ```python main.py```

### Raspberry configuration
Follow this steps to automatically run the project on Raspberry startup, assuming you have already cloned the repository.
* ```cd tweetpulse-object-python```
* Give shell script appropriate rights : ```chmod 755 launcher.sh```
* Create log dir : ```mkdir ~/tp_logs```
* Launch script on rasp startup :
  * edit crontab : ```sudo crontab -e```
  * add this line at the end of file : ```@reboot sh /home/pi/tweet-pulse-object-python/launcher.sh >/home/pi/logs_tp/cronlog 2>&1``` save and exit.
  * reboot the Raspberry
Et voila ! All the logs of the app will be saved in /home/pi/logs_tp/cronlog file, and the script will be launch on startup.

###Wifi setup
* Edit the configuration file : ```sudo nano /etc/wpa_supplicant/wpa_supplicant.conf``` and enter the ssid and pswd of your network.
* Then stop wlan0 : ```sudo wpa_action wlan0 stop```
* And relaunch : ```sudo ifup wlan0```

###TODO
* ~~Handle internet connexion/deco -> and availability of our server (what happend if unavailable ?)~~
* ~~Handle twitter api down~~
* On start:
  * ~~check if contain previous data or make first config~~
  * check previous data : is search expression the same ?
  * first config : take 6 last digit of MAC adress
* Handle last tweet searched timestamp
* Send stats datas to server every minutes

###Dev tips
I use a package called [remote-ftp](https://atom.io/packages/remote-ftp) for synchronise my local version of the code to the Raspberry.
