#!/bin/sh

cd /home/pi/tweet-pulse-object-python
# Launch intro script
python intro.py
# while impossible to ping web2.uzful.fr
while ! ping -c 1 -W 1 149.202.206.53; do
    # wait for internet connexion to start
    echo "Waiting for 149.202.206.53 - network interface might be down..."
    sleep 1
done
# launch main tweetpulse script
python main.py
