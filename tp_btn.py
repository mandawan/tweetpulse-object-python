import RPi.GPIO as GPIO
import time
import os
import subprocess
import settings
import tp_utils
from threading import Thread

class TPButton(Thread):
    """
    A manager to handle tweet pulse button
    """

    def __init__(self):
        Thread.__init__(self)
        GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def run(self):
        while True:
            input_state = GPIO.input(17)
            if input_state == False:
                os.system('pkill -9 -f led.py')
                settings.display.clean_display()
                settings.display.__init__()
                time.sleep(1)
                tp_utils.write_to_file(settings.DATA_FILES['mentions_counter'], str(0))
                settings.display.display_mention_to_search()
