#!/usr/bin/env python

import settings
import sys
from stats_manager import StatsManager
from twitter import Api

class TwitterStreamListener(object):
    """
    A listener handles tweets are the received from the stream.
    This is a basic listener that just prints received tweets to stdout.
    """

    def __init__(self):
        # INIT statistics manager
        self.stats_manager = StatsManager()
        # INIT twitter API
        self.api = Api(settings.consumer_key,
          settings.consumer_secret,
          settings.access_token,
          settings.access_token_secret)
        # INIT empty list for expression to search
        # because GetStreamFilter take a list for argument
        self.expression = []

    def tweet_stream_from_expression(self, expression, lang):
        # Add current expression to expression list
        self.expression.append(expression)
        print('expression to search : ', self.expression)
        # Launch API streaming
        # Endless loop: even if error with streaming API or whatever
        while True:
            print("twitter stream")
            settings.display.display_mention_to_search()
            # we will always try again to get tweet stream from it
            try:
                for tweet in self.api.GetStreamFilter(track=self.expression):
                    if(tweet['lang'] and tweet['lang'] == lang):
                        tweet_text = tweet['text']
                        print tweet_text.encode('utf-8')
                        self.stats_manager.new_mention()
            except:
                # display error on screen like "can't connect to twitter"
                print("error connecting to twitter")
                continue

stream = TwitterStreamListener()
stream.tweet_stream_from_expression(sys.argv[1], sys.argv[2])
