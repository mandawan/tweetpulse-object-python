#!/usr/bin/python
#--------------------------------------
# The display part of this code is based on the work of Matt Hawkins : http://www.raspberrypi-spy.co.uk/
#--------------------------------------

# The wiring for the LCD is as follows:
# 1 : GND
# 2 : 5V
# 3 : Contrast (0-5V)*
# 4 : RS (Register Select)
# 5 : R/W (Read Write)       - GROUND THIS PIN
# 6 : Enable or Strobe
# 7 : Data Bit 0             - NOT USED
# 8 : Data Bit 1             - NOT USED
# 9 : Data Bit 2             - NOT USED
# 10: Data Bit 3             - NOT USED
# 11: Data Bit 4
# 12: Data Bit 5
# 13: Data Bit 6
# 14: Data Bit 7
# 15: LCD Backlight +5V**
# 16: LCD Backlight GND

import RPi.GPIO as GPIO
import time
import settings
import tp_utils

class DisplayManager(object):
    """
    A manager to handle display on screen and physical interaction with the object
    """

    def __init__(self):

        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)       # Use BCM GPIO numbers
        GPIO.setup(settings.LCD_E, GPIO.OUT)  # E
        GPIO.setup(settings.LCD_RS, GPIO.OUT) # RS
        GPIO.setup(settings.LCD_D4, GPIO.OUT) # DB4
        GPIO.setup(settings.LCD_D5, GPIO.OUT) # DB5
        GPIO.setup(settings.LCD_D6, GPIO.OUT) # DB6
        GPIO.setup(settings.LCD_D7, GPIO.OUT) # DB7

        GPIO.setup(settings.stop_alert_btn, GPIO.IN, pull_up_down=GPIO.PUD_UP)

        GPIO.setup(settings.LED_ALERT, GPIO.OUT)
        GPIO.output(settings.LED_ALERT,True)
        self.input_state = GPIO.input(settings.stop_alert_btn)

        GPIO.setup(settings.stop_alert_btn, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        # GPIO.setmode(GPIO.BCM)
        # Initialise display
        self.lcd_init()

    def lcd_init(self):
      # Initialise display
      self.lcd_byte(0x33,settings.LCD_CMD) # 110011 Initialise
      self.lcd_byte(0x32,settings.LCD_CMD) # 110010 Initialise
      self.lcd_byte(0x06,settings.LCD_CMD) # 000110 Cursor move direction
      self.lcd_byte(0x0C,settings.LCD_CMD) # 001100 Display On,Cursor Off, Blink Off
      self.lcd_byte(0x28,settings.LCD_CMD) # 101000 Data length, number of lines, font size
      self.lcd_byte(0x01,settings.LCD_CMD) # 000001 Clear display
      time.sleep(settings.E_DELAY)

    def lcd_byte(self, bits, mode):
      # Send byte to data pins
      # bits = data
      # mode = True  for character
      #        False for command

      GPIO.output(settings.LCD_RS, mode) # RS

      # High bits
      GPIO.output(settings.LCD_D4, False)
      GPIO.output(settings.LCD_D5, False)
      GPIO.output(settings.LCD_D6, False)
      GPIO.output(settings.LCD_D7, False)
      if bits&0x10==0x10:
        GPIO.output(settings.LCD_D4, True)
      if bits&0x20==0x20:
        GPIO.output(settings.LCD_D5, True)
      if bits&0x40==0x40:
        GPIO.output(settings.LCD_D6, True)
      if bits&0x80==0x80:
        GPIO.output(settings.LCD_D7, True)

      # Toggle 'Enable' pin
      self.lcd_toggle_enable()

      # Low bits
      GPIO.output(settings.LCD_D4, False)
      GPIO.output(settings.LCD_D5, False)
      GPIO.output(settings.LCD_D6, False)
      GPIO.output(settings.LCD_D7, False)
      if bits&0x01==0x01:
        GPIO.output(settings.LCD_D4, True)
      if bits&0x02==0x02:
        GPIO.output(settings.LCD_D5, True)
      if bits&0x04==0x04:
        GPIO.output(settings.LCD_D6, True)
      if bits&0x08==0x08:
        GPIO.output(settings.LCD_D7, True)

      # Toggle 'Enable' pin
      self.lcd_toggle_enable()

    def lcd_toggle_enable(self):
      # Toggle enable
      time.sleep(settings.E_DELAY)
      GPIO.output(settings.LCD_E, True)
      time.sleep(settings.E_PULSE)
      GPIO.output(settings.LCD_E, False)
      time.sleep(settings.E_DELAY)

    def lcd_string(self, message, line):
      # Send string to display

      message = message.ljust(settings.LCD_WIDTH," ")

      self.lcd_byte(line, settings.LCD_CMD)

      for i in range(settings.LCD_WIDTH):
        self.lcd_byte(ord(message[i]),settings.LCD_CHR)

    def clean_display(self):
        self.lcd_byte(0x01, settings.LCD_CMD)
        GPIO.cleanup()

    def blink(self):
        for i in range(0,200):## Run loop numTimes
            GPIO.output(settings.LED_ALERT,False)## Switch on pin 7
            time.sleep(0.2)## Wait
            GPIO.output(settings.LED_ALERT,True)## Switch off pin 7
            time.sleep(0.2)## Wait
        GPIO.output(settings.LED_ALERT,True)

    def display_mention_to_search(self):
        mention = tp_utils.read_file(settings.DATA_FILES['mention_to_search'])
        self.lcd_string("Search : " + mention, settings.LCD_LINE_1)
