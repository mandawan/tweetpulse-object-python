#!/usr/bin/python

import tp_utils
import schedule
import time
import subprocess
import os
import settings


class CycleManager(object):
    """
    A manager to handle tweet pulse cycle
    """

    def launch_cycle(self):
        process = subprocess.Popen(["python","tp_cycle.py"])
        process.wait()
