#!/usr/bin/python

import os
from display_manager import DisplayManager

global display

# Get application path
APP_DIR = os.path.dirname(os.path.realpath(__file__))

###--- Constants ---###
# ID of current tweetpulse
CLIENT_ID = "F30F0A"

# Mqtt topics
DATA_TOPIC = "uzful_data/"+CLIENT_ID
INIT_TOPIC = "uzful_init/"+CLIENT_ID
CONTROL_TOPIC = "uzful_control/"+CLIENT_ID

# MQTT topics dictionnary to subscribe to
TOPICS = {
    'data' : DATA_TOPIC,
    'init' : INIT_TOPIC,
    'control' : CONTROL_TOPIC
}

# Data files
DATA_FILES = {
    'mentions_counter' : 'mentions_count.txt',
    'pulse_level' : 'pulse_level.txt',
    'last_mention_time' : 'last_mention_time.txt',
    'begin_cycle_time' : 'begin_cycle_time.txt',
    'mention_to_search' : 'mention_to_search.txt',
    'lang_to_search' : 'lang_to_search.txt'
}
###--- end constants --###

### Twitter ###

# Initialize global var
MENTION_COUNT = 0
BEGIN_CYCLE_TIME = 0

# Go to http://apps.twitter.com and create an app.
# The consumer key and secret will be generated for you after
consumer_key="iqk4ojMgHtSGmU6sibw0qie7a"
consumer_secret="FFFZYsoDMs06BWGOWtj6PAOFi6bsDKbhuZ7BI0zV7BL6VznzMf"

# After the step above, you will be redirected to your app's page.
# Create an access token under the the "Your access token" section
access_token="704958300027297792-zm6dWZ55KlHS50ueyVCBMrqMIdEZT06"
access_token_secret="ug7IyCRSA5nZOLfEPyTtUz23rSfgsezrGlVsxJaZkKPaM"

is_streaming = False

### end twitter ###

###--- GPIO ---###

### Screen display ###

# Define GPIO to LCD mapping
LCD_RS = 7
LCD_E  = 8
LCD_D4 = 25
LCD_D5 = 24
LCD_D6 = 23
LCD_D7 = 18

# Define some device constants
LCD_WIDTH = 16    # Maximum characters per line
LCD_CHR = True
LCD_CMD = False

LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line

# Timing constants
E_PULSE = 0.0005
E_DELAY = 0.0005


### Leds light ###

LED_ALERT = 4

stop_alert_btn = 17

###--- End GPIO ---###

display = DisplayManager()
