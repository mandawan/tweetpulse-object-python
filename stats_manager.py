#!/usr/bin/python

import os, time, calendar, datetime
import settings
import tp_utils

class StatsManager(object):
    """
    A manager to handle statistics from twitter
    """

    #def __init__(self):

    def save_mention_time(self):
        current_time_milli = tp_utils.get_current_time_with_milli()
        tp_utils.write_to_file(settings.DATA_FILES['last_mention_time'], str(current_time_milli))
        print "new mention time saved"

    def new_mention(self):
        print("new mention : save it")
        mentions_nb = tp_utils.read_file(settings.DATA_FILES['mentions_counter'])
        if (mentions_nb != ""):
            mentions_nb = int(mentions_nb) + 1
        else :
            mentions_nb = 1
        tp_utils.write_to_file(settings.DATA_FILES['mentions_counter'], str(mentions_nb))
        settings.MENTION_COUNT = mentions_nb
        print "mentions nb : ", mentions_nb
        self.save_mention_time()
        settings.display.lcd_string(str(mentions_nb) ,settings.LCD_LINE_2)
        tp_utils.check_alert()
